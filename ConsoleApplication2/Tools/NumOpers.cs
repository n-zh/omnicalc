﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmniCalc.Tools
{
    public class NumOpers
    {
        public static void Sum()
        {
            Console.WriteLine("Введите первое слагаемое:");
            string slag1 = Console.ReadLine();

            Console.WriteLine("Введите второе слагаемое:");
            string slag2 = Console.ReadLine();

            int intSlag1;
            int intSlag2;
            string result = "";

            if (Int32.TryParse(slag1, out intSlag1) && Int32.TryParse(slag2, out intSlag2))
            {
                result = String.Format($"Сумма = {intSlag1 + intSlag2}");

            }
            else
            {
                result = "Вы ввели некорректные слагаемые!";
            }

            Console.WriteLine(result);

            Console.ReadKey();
        }
        public static void Sub()
        {
            Console.WriteLine("Введите уменьшаемое:");
            string umensh = Console.ReadLine();

            Console.WriteLine("Введите вычитаемое:");
            string vych = Console.ReadLine();

            int intUmensh;
            int intVych;
            string result = "";

            if (Int32.TryParse(umensh, out intUmensh) && Int32.TryParse(vych, out intVych))
            {
                result = String.Format($"Разность = {intUmensh - intVych}");
            }
            else
            {
                result = "Вы ввели некорректные уменьшаемое или вычитаемое!";
            }

            Console.WriteLine(result);

            Console.ReadKey();
        }
        public static void Mult()
        {
            Console.WriteLine("Введите первый множитель:");
            string mnozh1 = Console.ReadLine();

            Console.WriteLine("Введите второй множитель:");
            string mnozh2 = Console.ReadLine();

            int intMnozh1;
            int intMnozh2;
            string result = "";

            if (Int32.TryParse(mnozh1, out intMnozh1) && Int32.TryParse(mnozh2, out intMnozh2))
            {
                result = String.Format($"Произвдение = {intMnozh1 * intMnozh2}");
            }
            else
            {
                result = "Вы ввели некорректные множители!";
            }

            Console.WriteLine(result);

            Console.ReadKey();
        }
        public static void Div()
        {
            Console.WriteLine("Введите делимое:");
            string del1 = Console.ReadLine();

            Console.WriteLine("Введите делитель:");
            string del2 = Console.ReadLine();

            int intDel1;
            int intDel2;
            string result = "";

            if (Int32.TryParse(del1, out intDel1) && Int32.TryParse(del2, out intDel2))
            {
                result = String.Format($"Частное = {intDel1 / intDel2}");
            }
            else
            {
                result = "Вы ввели некорректные делимое или делитель!";
            }
            Console.WriteLine(result);

            Console.ReadKey();
        }
    }
}
