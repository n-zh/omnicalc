﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OmniCalc.Tools.NumOpers;

namespace OmniCalc
{
    class Calculator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 1, если хотите складывать. \nВведите 2, если хотите вычитать. \nВведите 3, если хотите умножать. \nВведите 4, если хотите делить.");
            string sel = Console.ReadLine();

            switch (sel)
            {
                case "1":
                    Console.WriteLine();
                    Sum();
                    break;

                case "2":
                    Console.WriteLine();
                    Sub();
                    break;

                case "3":
                    Console.WriteLine();
                    Mult();
                    break;

                case "4":
                    Console.WriteLine();
                    Div();
                    break;

                default:
                    Console.WriteLine("Вы ввели неверное значение!");
                    Console.ReadKey();
                    break;
            }
        }
    }
}
